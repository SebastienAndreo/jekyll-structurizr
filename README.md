# jekyll-structurizr

The goal of this plugin is to facilitate the rendering of [structurizr DSL](https://github.com/structurizr/dsl) models in a Jekyll static website.
The usage quite simple in any markdown file, use the liquid tag `{% Structurizr %}` and `{% endStructurizr %}` to put into bracket your instance of the DSL.
During the generation of the static website, a SVG will generated and integrated to the HTML page.

For the model rendering the plugin support only :
- plantuml/structurizr


## Prerequisites, setting up your system

### Install plantuml.jar

Then, make sure PlantUML is installed on your build machine, and can be executed with a simple plantuml command.

For Linux user, you could create a /usr/bin/plantuml with contents:

```
#!/bin/bash

java -jar /home/user/Downloads/plantuml.jar "$1" "$2"
```
Remember to change the path to plantuml.jar file.

Then set executable permission.

```
chmod +x /usr/bin/plantuml
```

For Windows users, create a batch file plantuml.bat

```
@ECHO OFF
java  -jar c:\Tools\plantuml\plantuml.jar -tsvg %1 %2 
```
Remember to change the path to plantuml.jar file and add the bat to your %PATH%


### Install structurizr CLI

- Follow the installation of the [CLI](https://github.com/structurizr/cli/blob/master/docs/getting-started.md)

The plugin expect to call a script `structurizr` check that script in in your PATH.

```bash
seb@linux:~/$ structurizr
Structurizr CLI v1.9.0
Usage: structurizr push|pull|lock|unlock|export|validate [options]
```



## configuring in Jekyll 

Add this line to your Gemfile:

```ruby
group :jekyll_plugins do
  gem "jekyll-structurizr"
end
```

And then execute:


```bash
    $ bundle install
```

Alternatively install the gem yourself as:

```bash
    $ gem install jekyll-structurizr
```

and put this in your ``_config.yml``

```yaml
plugins:
    - jekyll-structurizr
```


# Release notes

### Version 0.1.0

- Basic generation supporting plantuml/structurizr

### Version 0.0.1

- Initial version, generation of an svg from a structurizr DSL syntax
