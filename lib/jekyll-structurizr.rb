require 'digest'
require 'fileutils'

module Jekyll
  class StructurizrBlock < Liquid::Block
    def initialize(tag_name, markup, tokens)
      super
      @html = (markup or '').strip
    end

    def render(context)
      site = context.registers[:site]
      name = Digest::MD5.hexdigest(super)
      if !File.exists?(File.join(site.dest, "c4/#{name}.svg"))
        outputdir = File.join(site.source, "c4")
        dsl = File.join(site.source, "c4/#{name}.dsl")
        defaultuml = File.join(site.source, "c4/structurizr-SystemContext.puml")
        uml = File.join(site.source, "c4/#{name}.uml")
        svg = File.join(site.source, "c4/#{name}.svg")
        if File.exists?(svg)
          puts "File #{svg} already exists (#{File.size(svg)} bytes)"
        else
          FileUtils.mkdir_p(File.dirname(dsl))
          File.open(dsl, 'w') { |f|
            @text = super  
          #  appendC4Beacon=true
          #  if  @text.include? "@starstructurizr"
          #    appendC4Beacon = false
          #    puts "stream already contains starstructurizr beacons"
          #  end
          #  if appendC4Beacon
          #    f.write("@starstructurizr\n")
          #  end
            f.write(super)
          #  if appendC4Beacon
          #    f.write("\n@endstructurizr")
          #  end
          }
          puts "Exec : structurizr export -f plantuml/c4plantuml -o #{outputdir} -w  #{dsl}"
          system("structurizr export -f plantuml/structurizr -o #{outputdir} -w  #{dsl}")
          File.rename(defaultuml, uml)
          puts "File #{uml} created (#{File.size(uml)} bytes)"
          puts "plantuml -tsvg #{uml}"
          system("plantuml -tsvg #{uml}")
          site.static_files << Jekyll::StaticFile.new(
            site, site.source, 'c4', "#{name}.svg"
          )
          puts "File #{svg} created (#{File.size(svg)} bytes)"
        end
      end
      "<div class='plantuml' style='width:fit-content'><object class='plantuml'  data='#{site.baseurl}/c4/#{name}.svg' type='image/svg+xml' #{@html}></object></div>"
    end
  end
end

Liquid::Template.register_tag('Structurizr', Jekyll::StructurizrBlock)